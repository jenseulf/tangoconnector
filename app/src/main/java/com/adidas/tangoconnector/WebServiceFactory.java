package com.adidas.tangoconnector;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by jenseulf on 3/8/16.
 */
public class WebServiceFactory {

    private String TAG = "WebServiceFactory";

    private static WebServiceFactory webServiceFactory;
    private static Context context;
    private static RequestQueue requestQueue;
    private static String serverUrl;

    private WebServiceFactory(Context c) {

        context = c.getApplicationContext();
        requestQueue = getRequestQueue();

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        serverUrl = sharedPref.getString("server_url", "");
        //serverUrl = "http://www.google.com";

        //Uncomment next line for the workaround of accepting all certificates
        //HttpsTrustManager.allowAllSSL();
    }

    public static synchronized WebServiceFactory getInstance(Context c) {

        if (webServiceFactory == null) {
            webServiceFactory = new WebServiceFactory(c);
        }

        return webServiceFactory;
    }

    private RequestQueue getRequestQueue() {

        if (requestQueue == null) {

            Cache cache = new DiskBasedCache(context.getCacheDir(), 1024 * 1024);
            Network network = new BasicNetwork(new HurlStack());
            requestQueue = new RequestQueue(cache, network);
            requestQueue.start();
        }

        return requestQueue;
    }

    public void stringGetRequest() {

        String thisUrl = serverUrl + "users";
        Log.i(TAG, "String request url: " + thisUrl);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, thisUrl,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "String Response: " + response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.w(TAG, "String Error: " + error.toString());
                    }
                });

        requestQueue.add(stringRequest);
    }

    public void getArray(Response.Listener<JSONArray> response, Response.ErrorListener error, String directory) {

        String thisUrl = serverUrl + directory;
        Log.i(TAG, "JSON request url: " + thisUrl);

           JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                   Request.Method.GET,
                   thisUrl,
                   null,
                   response,
                   error);

        requestQueue.add(jsonArrayRequest);
    }

    public void getObject(Response.Listener<JSONObject> response, Response.ErrorListener error, String directory) {

        String thisUrl = serverUrl + directory;
        Log.i(TAG, "JSON request url: " + thisUrl);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                thisUrl,
                null,
                response,
                error);

        requestQueue.add(jsonObjectRequest);
    }

    public void post(Response.Listener<JSONObject> response, Response.ErrorListener error, String directory, HashMap<String, String> hashmap){
        final HashMap<String, String> parameters = hashmap;

        String thisUrl = serverUrl + directory;
        Log.i(TAG, "JSON request url: " + thisUrl);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,
                thisUrl,
                new JSONObject(hashmap),
                response,
                error);

        requestQueue.add(jsonObjectRequest);
    }

    public void post(Response.Listener<JSONObject> response, Response.ErrorListener error, String directory, JSONObject jsonObject){
        String thisUrl = serverUrl + directory;
        Log.i(TAG, "JSON request url: " + thisUrl);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,
                thisUrl,
                jsonObject,
                response,
                error);

        requestQueue.add(jsonObjectRequest);
    }

    public void put(Response.Listener<JSONObject> response, Response.ErrorListener error, String directory, HashMap<String, String> hashmap){
        final HashMap<String, String> parameters = hashmap;

        String thisUrl = serverUrl + directory;
        Log.i(TAG, "JSON request url: " + thisUrl);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.PUT,
                thisUrl,
                new JSONObject(hashmap),
                response,
                error);

        requestQueue.add(jsonObjectRequest);
    }

    private <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }


}
