package com.adidas.tangoconnector;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final int USER_MANAGEMENT_REQUEST_CODE = 1;
    private static final int SENSOR_MANAGEMENT_REQUEST_CODE = 2;
    private static final int DATA_MANAGEMENT_REQUEST_CODE = 3;

    private TextView mainActionLog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mainActionLog = (TextView) findViewById(R.id.main_action_log_text_view_label);
        mainActionLog.setText("Start of action log:");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            startActivity(new Intent(this, SettingsActivity.class));

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_user_mngt) {
            startActivityForResult(new Intent(this, UserManagementActivity.class), USER_MANAGEMENT_REQUEST_CODE);
        }
        if (id == R.id.nav_sensor_mngt) {
            startActivityForResult(new Intent(this, SensorManagementActivity.class), SENSOR_MANAGEMENT_REQUEST_CODE);
        }
        if (id == R.id.nav_data_mngt) {
            startActivityForResult(new Intent(this, DataManagementActivity.class), DATA_MANAGEMENT_REQUEST_CODE);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK){
            switch(requestCode) {
                case USER_MANAGEMENT_REQUEST_CODE:
                    addToLog("User management:");
                    break;
                case SENSOR_MANAGEMENT_REQUEST_CODE:
                    addToLog("Sensor management:");
                    break;
                case DATA_MANAGEMENT_REQUEST_CODE:
                    addToLog("Data management:");
                    break;
                default:
                    addToLog("Unknown request performed!");
            }
            Bundle bundle = data.getExtras();
            if(bundle != null) {
                addToLog(bundle.getString("log"));
            }
            else{
                addToLog("No log information.");
            }
        }
        else{
            if (resultCode == RESULT_CANCELED){
                addToLog("Request canceled.");
            }
            else{
                addToLog("Request failed.");
            }

        }

        //super.onActivityResult(requestCode, resultCode, data);
    }

    private void addToLog(String message){
        mainActionLog.setText(mainActionLog.getText().toString() + "\n" + message);
    }
}
