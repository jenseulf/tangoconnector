package com.adidas.tangoconnector;

import java.util.Calendar;
import java.util.Random;
import java.util.TimeZone;

/**
 * Created by jenseulf on 3/16/16.
 */
public class RandomUtil {

    private static RandomUtil ourInstance = new RandomUtil();
    private static String allLetters[] = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};

    public static RandomUtil getInstance() {
        return ourInstance;
    }

    private RandomUtil() {

    }

    public String createRandomString(int stringLength){

        Random randomNumber = new Random();
        String randomString = "";

        for (int i = 0; i < stringLength; i++){

            randomString += allLetters[randomNumber.nextInt(allLetters.length)];
        }

        return randomString;
    }

    public String createGmtTimestamp(){
        long timeInMs;

        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getTimeZone("GMT"));
        timeInMs = cal.getTimeInMillis();

        return String.valueOf(timeInMs);
    }

    public int[] createRandomData(int dataSize){
        Random randomNumber = new Random();

        int randomData[] = new int[dataSize];

        for (int i = 0; i < dataSize; i++){

            randomData[i] = randomNumber.nextInt(100);
        }

        return randomData;
    }

}
