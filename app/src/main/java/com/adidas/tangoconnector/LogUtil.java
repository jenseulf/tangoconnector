package com.adidas.tangoconnector;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by jenseulf on 3/23/16.
 */
public class LogUtil {
    private static LogUtil ourInstance = new LogUtil();

    public static LogUtil getInstance() {
        return ourInstance;
    }

    private LogUtil() {
    }

    public String appendRequest(String currentString, String call, HashMap<String, String> request){
        String result = currentString + "\n";

        result += "Call: " + call + ", request\n";

        if(request != null){

            for (String key : request.keySet()) {
                result += key + ": " + request.get(key) + "\n";
            }
        }

        return result;
    }

    public String appendRequest(String currentString, String call, JSONObject jsonObject){
        String result = currentString + "\n";

        result += "Call: " + call + ", request\n";

        if(jsonObject != null){

            Iterator iter = jsonObject.keys();
            while (iter.hasNext()) {
                String key = (String) iter.next();
                try {
                    result += key + ": " + jsonObject.getString(key) + "\n";
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        return result;
    }

    public String appendRequest(String currentString, String call, String url){
        String result = currentString + "\n";

        result += "Call: " + call + ", request\n";

        result += "Url: " + url + "\n";

        return result;
    }



    public String appendResponse(String currentString, String call, JSONObject jsonObject){

        String result = currentString + "\n";

        result += "Call: " + call + ", response\n";

        if(jsonObject != null) {

            Iterator iter = jsonObject.keys();
            while (iter.hasNext()) {
                String key = (String) iter.next();
                try {
                    result += key + ": " + jsonObject.getString(key) + "\n";
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        return result;
    }

    public String appendError(String currentString, String call, VolleyError error){

        NetworkResponse networkResponse = error.networkResponse;

        String result = currentString + "\n";

        result += "Call: " + call + ", error\n";

        result += "Error: " + error.toString() + "\n";

        if (networkResponse != null) {

            result += "Http code: " + Integer.toString(networkResponse.statusCode) + "\n";

            switch (networkResponse.statusCode) {
                case (HttpURLConnection.HTTP_ACCEPTED):
                    result += "Description: " + "Request was accepted and will be processed." + "\n";
                    break;
                case (HttpURLConnection.HTTP_CONFLICT):
                    result += "Description: " + "Resource already exists." + "\n";
                    break;
                case (HttpURLConnection.HTTP_BAD_REQUEST):
                    result += "Description: " + "Request was malformed." + "\n";
                    break;
                default:
                    result += "Description: " + "Unknown result code." + "\n";
            }
        }
        else{
            result += "Description: " + "No Http result code available." + "\n";
        }

        return result;
    }

}
