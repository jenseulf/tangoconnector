package com.adidas.tangoconnector;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.UUID;

public class UserManagementActivity extends AppCompatActivity {

    String TAG = "UserManagementActivity";

    private String directory ="users";

    private EditText emailEditText;
    private EditText firstNameEditText;
    private EditText lastNameEditText;
    private EditText screenNameEditText;
    private TextView logTextView;

    String log = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_management);

        emailEditText = (EditText) findViewById(R.id.add_user_input_email);
        firstNameEditText = (EditText) findViewById(R.id.add_user_input_first_name);
        lastNameEditText = (EditText) findViewById(R.id.add_user_input_last_name);
        screenNameEditText = (EditText) findViewById(R.id.add_user_input_screen_name);
        logTextView = (TextView) findViewById(R.id.user_log);
    }

    public void onAddRealUserClick(View v){

        HashMap<String, String> user = createRealUser();

        addUser(v, user);
    }

    public void onAddVirtualUserClick(View v){
        HashMap<String, String> user = createVirtualUser();

        addUser(v, user);

    }

    public void onListUsersClick(View v){

        Log.i(TAG, "List users ");
        log = LogUtil.getInstance().appendRequest(log, "List user", directory);
        logTextView.setText("List users clicked.");

        Response.Listener<JSONArray> jsonArrayResponse = new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {


                Log.i(TAG, "List user response: " + response.toString());
                logTextView.setText("Download data success.");

                for (int i = 0; i < response.length(); i++) {
                    try {
                        log = LogUtil.getInstance().appendResponse(log, "List user", (JSONObject) response.get(i));
                    } catch (JSONException e) {
                        Log.w(TAG, "List user: Empty user.");
                    }
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i(TAG, "List user: " + error.toString());
                log = LogUtil.getInstance().appendError(log, "List user", error);
                logTextView.setText("List user failed.");
            }
        };

        WebServiceFactory.getInstance(this).getArray(jsonArrayResponse, error, directory);
    }

    public void onChangeUserClick(View v){

        String url = directory + "/" + getResources().getString(R.string.put_user_existing_id);

        HashMap<String, String> user = createVirtualUser();
        user.remove(getResources().getString(R.string.create_user_userId_label));
        user.put(getResources().getString(R.string.create_user_userId_label), new String("\"" + getResources().getString(R.string.put_user_existing_id) + "\""));

        Log.i(TAG, "Change user " + user.toString());
        log = LogUtil.getInstance().appendRequest(log, "Change user", user);
        logTextView.setText("Change user clicked.");

        Response.Listener<JSONObject> jsonObjectResponse = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i(TAG, "Change user " + response.toString());
                log = LogUtil.getInstance().appendResponse(log, "Change user", response);
                logTextView.setText("Change user success.");
            }
        };


        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.w(TAG,"Change user " + error.toString());
                log = LogUtil.getInstance().appendError(log, "Change user", error);
                logTextView.setText("Change user failed.");
            }
        };

        WebServiceFactory.getInstance(this).put(jsonObjectResponse, error, url, user);

    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent();
        intent.putExtra("log", log);
        setResult(RESULT_OK, intent);
        finish();

        //super.onBackPressed();
    }

    private void addUser(View v, HashMap<String, String> user){

        Log.i(TAG, "Add user " + user.toString());
        log = LogUtil.getInstance().appendRequest(log, "Add user", user);
        logTextView.setText("Add user clicked.");

        final Response.Listener<JSONObject> jsonObjectResponse = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.i(TAG,"Add user " + response.toString());
                log = LogUtil.getInstance().appendResponse(log, "Add user", response);
                logTextView.setText("Add user success.");

            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.w(TAG, "Add user " + error.toString());
                log = LogUtil.getInstance().appendError(log, "Add user", error);
                logTextView.setText("Add user failed.");
            }
        };

        WebServiceFactory.getInstance(this).post(jsonObjectResponse, error, directory, user);

    }

    private HashMap<String, String> createVirtualUser(){

        String randomString = RandomUtil.getInstance().createRandomString(5);
        String randomID = UUID.randomUUID().toString();

        HashMap<String, String> input = new HashMap<String, String>();
        input.put(getResources().getString(R.string.create_user_userId_label), new String( "\"" + randomID + "\""));
        input.put(getResources().getString(R.string.create_user_firstName_label), new String( "\"" + randomString + "\""));
        input.put(getResources().getString(R.string.create_user_lastName_label), new String("\"" + randomString + "\""));
        input.put(getResources().getString(R.string.create_user_screenName_label), new String("\"" + randomString + "\""));
        input.put(getResources().getString(R.string.create_user_email_label), new String("\"" + randomString + "@email.com" + "\""));
        input.put(getResources().getString(R.string.create_user_micoachId_label), getResources().getString(R.string.create_user_micoachId_value));
        input.put(getResources().getString(R.string.create_user_fromMicoach_label), getResources().getString(R.string.create_user_fromMicoach_value));

        return input;
    }

    private HashMap<String, String> createRealUser(){

        String randomID = UUID.randomUUID().toString();

        HashMap<String, String> input = new HashMap<String, String>();
        input.put(getResources().getString(R.string.create_user_userId_label), new String( "\"" + randomID + "\""));
        input.put(getResources().getString(R.string.create_user_firstName_label), new String( "\"" + firstNameEditText.getText() + "\""));
        input.put(getResources().getString(R.string.create_user_lastName_label), new String("\"" + lastNameEditText.getText() + "\""));
        input.put(getResources().getString(R.string.create_user_screenName_label), new String("\"" + screenNameEditText.getText() + "\""));
        input.put(getResources().getString(R.string.create_user_email_label), new String("\"" + emailEditText.getText() + "\""));
        input.put(getResources().getString(R.string.create_user_micoachId_label), getResources().getString(R.string.create_user_micoachId_value));
        input.put(getResources().getString(R.string.create_user_fromMicoach_label), getResources().getString(R.string.create_user_fromMicoach_value));

        return input;

    }


}


