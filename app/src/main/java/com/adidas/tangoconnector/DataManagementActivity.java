package com.adidas.tangoconnector;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

public class DataManagementActivity extends AppCompatActivity {

    private static final String TAG = "DataManagementActiv";

    private String directory = "sensorStreamChunk";

    private EditText sensorIdEditText;
    private EditText userIdEditText;
    private EditText dataEditText;
    private TextView dataTextView;
    private TextView logTextView;

    String chunkId = null;
    private int[] randomData;
    String log = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_management);

        sensorIdEditText = (EditText) findViewById(R.id.upload_data_input_sensorId);
        userIdEditText = (EditText) findViewById(R.id.upload_data_input_userId);
        dataEditText = (EditText) findViewById(R.id.upload_data_input_data);
        dataTextView = (TextView) findViewById(R.id.upload_data_output_data);
        logTextView = (TextView) findViewById(R.id.data_log);

        randomData = RandomUtil.getInstance().createRandomData(300);
        dataEditText.setText(Arrays.toString(randomData));
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent();
        intent.putExtra("log", log);
        setResult(RESULT_OK, intent);
        finish();

        //super.onBackPressed();
    }

    public void onUploadData(View v){

        HashMap<String, String> data = new HashMap<String,String>();
        data.put(getResources().getString(R.string.upload_data_sensorId_label), sensorIdEditText.getText().toString());
        data.put(getResources().getString(R.string.upload_data_userId_label), userIdEditText.getText().toString());
        data.put(getResources().getString(R.string.upload_data_data_label), dataEditText.getText().toString());

        JSONObject jsonObject = new JSONObject(data);
        try {
            jsonObject.put(getResources().getString(R.string.upload_data_frequency_label), Integer.parseInt(getResources().getString(R.string.upload_data_frequency_value)));
            JSONArray jsonArray = new JSONArray(randomData);

            jsonObject.put(getResources().getString(R.string.upload_data_data_label), jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i(TAG, "Upload data " + jsonObject.toString());
        log = LogUtil.getInstance().appendRequest(log, "Upload data", jsonObject);
        logTextView.setText("Upload data clicked.");

        Response.Listener<JSONObject> jsonObjectResponse = new Response.Listener<JSONObject>(){

            @Override
            public void onResponse(JSONObject response) {

                Log.i(TAG, "Upload data " + response.toString());
                log = LogUtil.getInstance().appendResponse(log, "Upload data", response);
                logTextView.setText("Upload data success.");

                try {
                    chunkId = response.getString("id");
                    Log.w(TAG, "Chunk id extracted.");
                    logTextView.setText(logTextView.getText() + "Chunk id: " + chunkId);
                } catch (JSONException e) {
                    Log.w(TAG, "No chunk id found.");
                    logTextView.setText(logTextView.getText() + "No chunk id returned!");
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener(){

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.w(TAG, "Upload data " + error.toString());
                log = LogUtil.getInstance().appendError(log, "Upload data", error);
                logTextView.setText("Data upload failed.");
            }
        };

        WebServiceFactory.getInstance(this).post(jsonObjectResponse, error, directory, jsonObject);
    }

    public void onDownloadData(View v){

        if(chunkId == null){
            return;
        }

        String url = directory + "/" + chunkId;
        Log.i(TAG, "Download data " + url);
        log = LogUtil.getInstance().appendRequest(log, "Download data", url);
        logTextView.setText("Download data clicked.");

        Response.Listener<JSONObject> jsonObjectResponse = new Response.Listener<JSONObject>(){

            @Override
            public void onResponse(JSONObject response) {
                Log.i(TAG, "Download data response: " + response.toString());
                log = LogUtil.getInstance().appendResponse(log, "Download data", response);
                logTextView.setText("Download data success.");

                String downloadData = "";

                try {
                    downloadData = response.getString("rawStream");
                    Log.i(TAG, "Downloaded data extracted.");
                    dataTextView.setText("\n" + downloadData);
                    logTextView.setText(logTextView.getText() + "Data received.");
                } catch (JSONException e) {
                    Log.w(TAG, "No data found.");
                    logTextView.setText(logTextView.getText() + "No data received.");
                }

            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG, "Download data: " + error.toString());
                log = LogUtil.getInstance().appendError(log, "Download data", error);
                logTextView.setText("Download data failed.");
            }
        };

        WebServiceFactory.getInstance(this).getObject(jsonObjectResponse, error, url);
    }

}
