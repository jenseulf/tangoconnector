package com.adidas.tangoconnector;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.UUID;

public class SensorManagementActivity extends AppCompatActivity {

    private static final String TAG = "SensorManagementAct";

    private String directory = "sensors";

    private EditText abbreviationEditText;
    private EditText nameEditText;
    private EditText descriptionEditText;
    private EditText unitEditText;
    private TextView logTextView;

    String log = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor_management);

        abbreviationEditText = (EditText) findViewById(R.id.add_sensor_input_abbreviation);
        nameEditText = (EditText) findViewById(R.id.add_sensor_input_name);
        descriptionEditText = (EditText) findViewById(R.id.add_sensor_input_description);
        unitEditText = (EditText) findViewById(R.id.add_sensor_input_unit);
        logTextView = (TextView) findViewById(R.id.sensor_log);
    }

    public void onChangeSensorClick(View v) {

        String url = directory + "/" + getResources().getString(R.string.change_sensor_existing_id);

        HashMap<String, String> user = createVirtualSensor();
        user.remove(getResources().getString(R.string.add_sensor_sensorId_label));
        user.put(getResources().getString(R.string.add_sensor_sensorId_label), new String("\"" + getResources().getString(R.string.change_sensor_existing_id) + "\""));

        Log.i(TAG, "Change sensor " + user.toString());
        log = LogUtil.getInstance().appendRequest(log, "Change sensor", user);
        logTextView.setText("Change sensor clicked.");

        Response.Listener<JSONObject> jsonObjectResponse = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i(TAG, "Change sensor " + response.toString());
                log = LogUtil.getInstance().appendResponse(log, "Change sensor", response);
                logTextView.setText("Change sensor success.");
            }
        };


        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.w(TAG, "Change sensor " + error.toString());
                log = LogUtil.getInstance().appendError(log, "Change sensor", error);
                logTextView.setText("Change sensor failed.");
            }
        };

        WebServiceFactory.getInstance(this).put(jsonObjectResponse, error, url, user);
    }

    public void onAddVirtualSensorClick(View v) {
        HashMap<String, String> sensor = createVirtualSensor();

        addSensor(v, sensor);
    }

    public void onAddRealSensorClick(View v) {
        HashMap<String, String> sensor = createRealSensor();

        addSensor(v, sensor);
    }

    public void onShowSensorDetailsClick(View v) {

        String url = directory + "/" + getResources().getString(R.string.change_sensor_existing_id);

        Log.i(TAG, "Show sensor " + url);
        log = LogUtil.getInstance().appendRequest(log, "Show sensor", url);
        logTextView.setText("Show sensor clicked.");

        Response.Listener<JSONObject> jsonObjectResponse = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.i(TAG, "Show sensor " + response.toString());
                log = LogUtil.getInstance().appendResponse(log, "Show sensor", response);
                logTextView.setText("Show sensor success.");
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.w(TAG, "Show sensor " + error.toString());
                log = LogUtil.getInstance().appendError(log, "Show sensor", error);
                logTextView.setText("Show sensor failed.");
            }
        };

        WebServiceFactory.getInstance(this).getObject(jsonObjectResponse, error, url);
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent();
        intent.putExtra("log", log);
        setResult(RESULT_OK, intent);
        finish();

        //super.onBackPressed();
    }

    private void addSensor(View v, HashMap<String, String> sensor) {

        Log.i(TAG, "Add sensor " + sensor.toString());
        log = LogUtil.getInstance().appendRequest(log, "Add sensor", sensor);
        logTextView.setText("Add sensor clicked.");

        Response.Listener<JSONObject> jsonObjectResponse = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.i(TAG, "Add sensor:+ " + response.toString());
                log = LogUtil.getInstance().appendResponse(log, "Add sensor", response);
                logTextView.setText("Add sensor success.");

            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.w(TAG, "Add sensor " + error.toString());
                log = LogUtil.getInstance().appendError(log, "Add sensor", error);
                logTextView.setText("Add sensor failed.");

            }
        };

        WebServiceFactory.getInstance(this).post(jsonObjectResponse, error, directory, sensor);
    }

    private HashMap<String, String> createVirtualSensor() {

        String randomString = RandomUtil.getInstance().createRandomString(5);
        String randomID = UUID.randomUUID().toString();

        HashMap<String, String> input = new HashMap<String, String>();
        input.put(getResources().getString(R.string.add_sensor_sensorId_label), new String("\"" + randomID + "\""));
        input.put(getResources().getString(R.string.add_sensor_abbreviation_label), new String("\"" + randomString + "\""));
        input.put(getResources().getString(R.string.add_sensor_description_label), new String("\"" + randomString + "\""));
        input.put(getResources().getString(R.string.add_sensor_name_label), new String("\"" + randomString + "\""));
        input.put(getResources().getString(R.string.add_sensor_units_label), new String("\"" + randomString + "\""));

        return input;
    }

    private HashMap<String, String> createRealSensor() {

        String randomID = UUID.randomUUID().toString();

        HashMap<String, String> input = new HashMap<String, String>();
        input.put(getResources().getString(R.string.add_sensor_sensorId_label), new String("\"" + randomID + "\""));
        input.put(getResources().getString(R.string.add_sensor_abbreviation_label), new String("\"" + abbreviationEditText.getText() + "\""));
        input.put(getResources().getString(R.string.add_sensor_description_label), new String("\"" + descriptionEditText.getText() + "\""));
        input.put(getResources().getString(R.string.add_sensor_name_label), new String("\"" + nameEditText.getText() + "\""));
        input.put(getResources().getString(R.string.add_sensor_units_label), new String("\"" + unitEditText.getText() + "\""));

        return input;

    }

}
